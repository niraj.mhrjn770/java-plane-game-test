import javax.swing.UIManager;
import resources.GamePanel;
import javax.swing.*;

public class MainActivity extends JFrame {
    MainActivity() {
        GamePanel game = new GamePanel();

        setTitle("App");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setFocusableWindowState(true);
        setAlwaysOnTop(true);
        add(game);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);

        game.gameStart();
    }

    public static void main(String[] args) throws Exception {
        System.out.println("MainActivity.main()");
        UIManager.setLookAndFeel("com.formdev.flatlaf.FlatDarculaLaf");
        new MainActivity();
    }
}
