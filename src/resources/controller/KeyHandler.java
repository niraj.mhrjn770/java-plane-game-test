package resources.controller;

import java.awt.event.*;

import resources.GamePanel;

public class KeyHandler implements KeyListener {
    GamePanel gp;
    public boolean up, right, left, down, space;

    public KeyHandler(GamePanel gp) {
        this.gp = gp;
        up = right = left = down = space = false;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                up = true;
                break;

            case KeyEvent.VK_LEFT:
                left = true;
                break;

            case KeyEvent.VK_RIGHT:
                right = true;
                break;

            case KeyEvent.VK_DOWN:
                down = true;
                break;

            case KeyEvent.VK_SPACE:
                space = true;
                break;

            case KeyEvent.VK_ESCAPE:
                System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                up = false;
                break;

            case KeyEvent.VK_LEFT:
                left = false;
                break;

            case KeyEvent.VK_RIGHT:
                right = false;
                break;

            case KeyEvent.VK_DOWN:
                down = false;
                break;

            case KeyEvent.VK_SPACE:
                space = false;
                break;
        }
    }
}
