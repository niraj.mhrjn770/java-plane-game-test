package resources.controller;

import java.util.LinkedList;
import java.awt.Graphics2D;

import resources.GamePanel;
import resources.entities.Bullet;

public class BulletController {
    GamePanel gp;

    private LinkedList<Bullet> b = new LinkedList<>();
    Bullet temp_bullet;

    public BulletController(GamePanel gp) {
        this.gp = gp;
    }

    public void tick() {
        for (int i = 0; i < b.size(); i++) {
            temp_bullet = b.get(i);
            temp_bullet.update();
            addBullet(temp_bullet);
            System.out.println("BulletController.tick()");
        }
    }

    public void render(Graphics2D g) {
        for (int i = 0; i < b.size(); i++) {
            temp_bullet = b.get(i);
            temp_bullet.drawBullet(g);
            System.out.println("BulletController.render()");
        }
    }

    public void addBullet(Bullet bullet) {
        System.out.println("BulletController.addBullet()");
        b.add(bullet);
    }

    public void removeBullet(Bullet bullet) {
        b.remove(bullet);
    }
}
