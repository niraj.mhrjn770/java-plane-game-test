package resources.entities;

import resources.GamePanel;

import java.awt.*;
import java.io.File;

import javax.swing.JOptionPane;
import javax.imageio.ImageIO;

public class Bullet extends Entity {
    GamePanel gp;

    public Bullet(GamePanel gp) {
        this.gp = gp;

        try {
            right_booster = ImageIO.read(new File("./src/resources/images/object/plane_missile.png"));
            right_booster1 = ImageIO.read(new File("./src/resources/images/object/plane_missile1.png"));
        } catch (Exception e) {
            int result = JOptionPane.showConfirmDialog(gp, "Error loading Bullet: " + e.getMessage(), "Confirm to exit",
                    JOptionPane.DEFAULT_OPTION);

            if (result == 0)
                System.exit(0);
        }
    }

    public void setAxis(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void update() {
        x -= 5;

        count++;

        if (count > 5) {
            if (num == 1)
                num = 2;
            else if (num == 2)
                num = 1;
            count = 0;
        }

        // System.out.println("Bullet.update(): " + x);
    }

    public void drawBullet(Graphics2D g) {
        if (num == 1)
            g.drawImage(right_booster, x, y, null);
        else
            g.drawImage(right_booster1, x, y, null);
    }
}
