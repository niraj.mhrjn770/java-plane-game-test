package resources.entities;

import resources.GamePanel;
import resources.controller.*;

import javax.swing.JOptionPane;
import javax.imageio.ImageIO;

import java.io.File;
import java.awt.*;

public class Player extends Entity {
    GamePanel gp;
    KeyHandler kh;

    Bullet bullet = new Bullet(gp);
    BulletController controller = new BulletController(gp);

    boolean speedUp;

    public boolean ready, shoot = false;

    public Player(GamePanel gp, KeyHandler kh) {
        this.gp = gp;
        this.kh = kh;

        x = y = 200;

        speed = 6;

        try {
            right = ImageIO.read(new File(".\\src\\resources\\images\\player\\plane_right.png"));
            right_booster = ImageIO.read(new File(".\\src\\resources\\images\\player\\plane_right_booster.png"));
            right_booster1 = ImageIO.read(new File(".\\src\\resources\\images\\player\\plane_right_booster2.png"));
        } catch (Exception e) {
            int result = JOptionPane.showConfirmDialog(gp, "Error loading player: " + e.getMessage(),
                    "Confirm Message", JOptionPane.DEFAULT_OPTION);
            if (result == 0)
                System.exit(0);
        }
    }

    public void drawPlane(Graphics2D g) {
        bullet.setAxis(30, 30);
        bullet.drawBullet(g);

        if (kh.right && speedUp) {
            if (num == 1)
                g.drawImage(right_booster, x, y, null);
            else
                g.drawImage(right_booster1, x, y, null);
        } else if (kh.down && speedUp) {
            if (num == 1)
                g.drawImage(right_booster, x, y, null);
            else
                g.drawImage(right_booster1, x, y, null);
        } else if (kh.down) {
            g.drawImage(right, x, y, null);
        } else {
            g.drawImage(right, x, y, null);
        }
    }

    public void update() {
        bullet.update();
        controller.tick();

        setMove();

        if (y > gp.height - gp.tile_size)
            y = gp.height - gp.tile_size;

        if (x > 615)
            x = 615;

        if (y < 0)
            y = 0;

        if (x < -30)
            x = -30;

        if (kh.space) {
            controller.addBullet(bullet);
        }

        count++;

        if (count > 8) {
            if (num == 1)
                num = 2;
            else if (num == 2)
                num = 1;
            count = 0;
        }
    }

    public void setMove() {
        moveRight();
        moveLeft();
        moveDown();
        moveUp();
    }

    protected void moveRight() {
        if (kh.right) {
            speedUp = true;
            x += speed + 2;
        }
    }

    protected void moveLeft() {
        if (kh.left) {
            speedUp = false;
            x -= speed;
        }
    }

    protected void moveDown() {
        if (kh.down) {
            speedUp = false;
            y += speed;
        }
        if (kh.down && kh.right) {
            x += speed + 1;
            speedUp = true;
        }
    }

    protected void moveUp() {
        if (kh.up) {
            speedUp = false;
            y -= speed;
        }
        if (kh.up && kh.right) {
            x += speed + 1;
            speedUp = true;
        }
    }

    public void shoot() {
        if (kh.space) {
            if (bullet == null)
                ready = true;
        }
    }

}
