package resources;

import java.awt.*;
import javax.swing.*;

import resources.controller.*;
import resources.entities.Player;
import resources.world.World;

public class GamePanel extends JPanel implements Runnable {
    private final int original_tile_size = 16, scale = 3;

    public final int tile_size = original_tile_size * scale,
            max_screen_rows = 15,
            max_screen_columns = 15;

    public final int width = max_screen_columns * tile_size,
            height = max_screen_rows * tile_size;

    Thread thread;

    KeyHandler keyHandler = new KeyHandler(this);

    Player player = new Player(this, keyHandler);

    World world = new World(this);

    BulletController controller = new BulletController(this);

    public GamePanel() {
        setPreferredSize(new Dimension(width, height));
        setBackground(Color.BLACK);
        addKeyListener(keyHandler);
        setDoubleBuffered(true);
        setFocusable(true);
        requestFocusInWindow();
    }

    public void gameStart() {
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        double drawInterval = 1000000000 / 60;
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;

        try {
            while (thread != null) {
                currentTime = System.nanoTime();
                delta += (currentTime - lastTime) / drawInterval;
                lastTime = currentTime;

                if (delta > 0) {
                    player.update();
                    repaint();
                    delta--;
                }
            }
        } catch (Exception e) {
            int result = JOptionPane.showConfirmDialog(this, "Error loading game, " + e.getMessage(), "Confirm Quit",
                    JOptionPane.DEFAULT_OPTION);
            if (result == 0)
                System.exit(0);
        }

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        world.setBackground(g2);
        player.drawPlane(g2);
        controller.render(g2);

        g2.dispose();
        g.dispose();
    }

}
