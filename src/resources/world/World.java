package resources.world;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.*;
import javax.imageio.ImageIO;

import resources.GamePanel;

public class World {
    GamePanel gp;

    BufferedImage background;

    public World(GamePanel gp) {
        this.gp = gp;

        try {
            background = ImageIO.read(new File(".\\src\\resources\\images\\object\\sky.png"));
        } catch (Exception e) {
            int result = JOptionPane.showConfirmDialog(gp, "Error loading Background: " + e.getMessage(),
                    "Confirm Message", JOptionPane.DEFAULT_OPTION);
            if (result == 0)
                System.exit(0);
        }
    }

    public void setBackground(Graphics2D g) {
        g.drawImage(background, 0, 0, gp.width, gp.height, null);
    }
}
